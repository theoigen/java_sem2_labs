package FileSystem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Directory extends Entity {
    private static final int DIR_MAX_ELEMS = 25;
    private Map<String, Entity> children = new HashMap<>(DIR_MAX_ELEMS);

    private Directory() {
        super("root");
    }

    private Directory(String name, Directory parent) {
        super(name, parent);
    }

    protected boolean containsChild(String childName) {
        return children.containsKey(childName);
    }

    protected int countChildren() {
        return children.size();
    }

    protected Entity getChild(String childName) {
        return children.get(childName);
    }

    protected void addChild(Entity child) {
        if (countChildren() >= DIR_MAX_ELEMS) {
            throw new IllegalArgumentException("Too many elements in directory");
        }
        children.put(child.getName(), child);
    }

    protected Entity removeChild(String childName) {
        return children.remove(childName);
    }

    protected void updateChild(Entity child, String oldName) {
        children.put(child.getName(), removeChild(oldName));
    }

    protected List<Entity> getChildren() {
        return new ArrayList<>(children.values());
    }

    private void throwExceptionIfDirIsRoot() {
        if (getParent() == null) {
            throw new IllegalArgumentException("Root directory couldn't move");
        }
    }

    public static Directory create(String name, Directory parent) {
        if (parent == null) {
            return new Directory();
        }
        return new Directory(name, parent);
    }

    public List<Entity> list() {
        return getChildren();
    }

    @Override
    public void delete() {
        if (countChildren() != 0) {
            throw new IllegalArgumentException("Dir not empty");
        }
        super.delete();
    }

    @Override
    public void move(Directory destination) {
        throwExceptionIfDirIsRoot();
        super.move(destination);
    }

    @Override
    public void move(String destination) {
        throwExceptionIfDirIsRoot();
        super.move(destination);
    }

    @Override
    public EntityType getType() {
        return EntityType.DIRECTORY;
    }

    @Override
    public boolean isDirectory() {
        return true;
    }
}
