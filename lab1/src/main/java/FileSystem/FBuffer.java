package FileSystem;
import java.util.Stack;

public class FBuffer<T> extends Entity {
    private final static int MAX_BUF_FILE_SIZE = 10;
    private Stack<T> stack;

    private FBuffer(String name, Directory parent) {
        super(name, parent);
        stack = new Stack<>();
    }

    public static <T> FBuffer<T> create(String name, Directory parent) {
        return new FBuffer<>(name, parent);
    }

    public void push(T element) {
        if (element == null) {
            throw new IllegalArgumentException("Element could not be null");
        }
        if (stack.size() >= MAX_BUF_FILE_SIZE) {
            throw new IllegalStateException("Too many elements in buffer");
        }
        stack.push(element);
    }

    public T consume() {
        return stack.pop();
    }

    @Override
    public EntityType getType() {
        return EntityType.BUFFER_FILE;
    }
}
