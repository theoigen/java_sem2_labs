package FileSystem;

public class Pipe extends Entity {
    private String data;

    private Pipe(String name, Directory parent) {
        super(name, parent);
        data = "";
    }

    public static Pipe create(String name, Directory parent) {
        return new Pipe(name, parent);
    }

    public void write(String data) {
        if (data == null) {
            throw new IllegalArgumentException("Data should not be null");
        }
        this.data = data;
    }

    public String read() {
        return data;
    }

    public void rename(String name) {
        setName(name);
    }

    @Override
    public EntityType getType() {
        return EntityType.NAMED_PIPE;
    }
}
