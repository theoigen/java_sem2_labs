package FileSystem;

public class TFileLog extends Entity {
    private StringBuffer data;

    private TFileLog(String name, Directory parent, String data) {
        super(name, parent);
        this.data = new StringBuffer(data);
    }

    public static TFileLog create(String name, Directory parent, String data) {
        if (data == null) {
            throw new IllegalArgumentException("Data should not be null");
        }
        return new TFileLog(name, parent, data);
    }

    public String read() {
        return data.toString();
    }

    public void append(String line) {
        if (line == null) {
            throw new IllegalArgumentException("Line should not be null");
        }
        data.append(line);
    }

    @Override
    public EntityType getType() {
        return EntityType.LOG_TEXT_FILE;
    }
}
