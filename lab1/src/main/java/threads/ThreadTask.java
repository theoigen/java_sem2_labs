package threads;

public interface ThreadTask extends Runnable {
    void task_init();
    void task();
    void task_finalize();
}
