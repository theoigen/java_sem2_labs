import FileSystem.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.nio.file.DirectoryNotEmptyException;
import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Directory")
public class DirectoryTest {
    @Test
    @DisplayName("Create directory")
    void createDirectoryTest() {
        assertThrows(IllegalArgumentException.class,
                () -> Directory.create("", Directory.create("test", null)));

        Directory dir = Directory.create("some", null);
        assertNotNull(dir);

        assertEquals(Entity.EntityType.DIRECTORY, dir.getType());
        assertEquals("root", dir.getName());
        assertTrue(dir.isDirectory());

        Directory subDir = Directory.create("tmp", dir);
        assertEquals("tmp", subDir.getName());
    }

    @Test
    @DisplayName("list directory")
    void listDirectoryTest() {
        Directory root = Directory.create("", null);
        ArrayList<Entity> listElements = new ArrayList<Entity>();
        listElements.add(Directory.create("sub", root));
        listElements.add(BinaryFile.create("file.bin", root, "data".getBytes()));
        listElements.add(TFileLog.create("log", root, "line"));
        assertEquals(listElements, root.list());
        assertEquals(Collections.emptyList(), Directory.create("", null).list());
    }

    @Test
    @DisplayName("delete directory")
    void deleteDirectoryTest() {
        Directory root = Directory.create("", null);
        BinaryFile file = BinaryFile.create("file", root, "data".getBytes());
        Directory subDir = Directory.create("var", root);

        assertThrows(IllegalArgumentException.class, root::delete);
        assertDoesNotThrow(subDir::delete);
        assertDoesNotThrow(file::delete);
        assertDoesNotThrow(root::delete);
    }

    @Test
    @DisplayName("move directory")
    void moveSubdirectoriesAndFilesTest() {
        Directory root = Directory.create("", null);
        Directory subDir = Directory.create("folder", root);
        Directory anotherSubDir = Directory.create("dir", root);
        TFileLog log = TFileLog.create("l.txt", subDir, "\n");

        assertThrows(IllegalArgumentException.class, () -> root.move(subDir));
        assertDoesNotThrow(() -> anotherSubDir.move("/" + subDir.getName()));
        assertTrue(subDir.list().contains(anotherSubDir));
        assertTrue(subDir.list().contains(log));
    }

    @Test
    @DisplayName("Add many entities to directory")
    void addManyEntitiesToDirTest() {
        Directory root = Directory.create("", null);

        for (int i = 0; i < 25; i++) {
            FBuffer.create("name" + i, root);
        }
        assertEquals(25, root.list().size());
        assertThrows(IllegalArgumentException.class, () -> FBuffer.create("test", root));
    }
}
