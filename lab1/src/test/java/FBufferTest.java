import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import FileSystem.FBuffer;
import FileSystem.Directory;
import FileSystem.Entity;

import java.util.EmptyStackException;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Buffer file")
public class FBufferTest {
    @Test
    @DisplayName("Create file")
    void createFileTest() {
        assertThrows(IllegalArgumentException.class, () -> FBuffer.create("test", null));

        Directory dir = Directory.create("some", null);
        assertThrows(IllegalArgumentException.class, () -> FBuffer.create("name/name", dir));
        assertThrows(IllegalArgumentException.class, () -> FBuffer.create("", dir));

        FBuffer file = FBuffer.create("file sample", dir);
        assertNotNull(file);
        assertEquals(Entity.EntityType.BUFFER_FILE, file.getType());
        assertEquals("file sample", file.getName());
        assertFalse(file.isDirectory());
    }

    @Test
    @DisplayName("Consume and push")
    void consumePushTest() {
        Directory dir = Directory.create("dir", null);
        FBuffer file = FBuffer.<Character>create("file", dir);

        assertThrows(EmptyStackException.class, file::consume);
        file.push('Q');
        file.push('A');
        assertEquals(Character.valueOf('A'), file.consume());
        assertEquals(Character.valueOf('Q'), file.consume());
        assertThrows(EmptyStackException.class, file::consume);

        assertThrows(IllegalArgumentException.class, () -> file.push(null));
    }

    @Test
    @DisplayName("Push many elements")
    void pushManyElementsTest() {
        Directory dir = Directory.create("dir", null);
        FBuffer file = FBuffer.<Character>create("file", dir);

        for (int i = 0; i < 10; i++) {
            file.push((char) ('A' + i));
        }
        assertThrows(IllegalStateException.class, () -> file.push('Z'));
    }
}
