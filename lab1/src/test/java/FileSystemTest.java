import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import FileSystem.Directory;
import FileSystem.Pipe;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("File system")
public class FileSystemTest {
    @Test
    @DisplayName("Move files")
    void moveFilesTest() {
        Directory root = Directory.create("", null);
        Pipe namedPipe = Pipe.create("pipe", root);
        Directory homeDir = Directory.create("home", root);
        Directory userDir = Directory.create("user", homeDir);

        namedPipe.move("/home/user/");
        assertFalse(userDir.list().isEmpty());
        assertEquals(namedPipe, userDir.list().get(0));

        userDir.move("/");
        assertTrue(root.list().contains(userDir));
    }

    @Test
    @DisplayName("Delete files")
    void deleteFilesTest() {
        Directory root = Directory.create("", null);
        Directory homeDir = Directory.create("home", root);
        Pipe namedPipe = Pipe.create("pipe", homeDir);
        Directory tempDir = Directory.create("user", homeDir);

        namedPipe.delete();
        assertFalse(homeDir.list().contains(namedPipe));
        assertTrue(homeDir.list().contains(tempDir));
        tempDir.delete();
        assertFalse(homeDir.list().contains(tempDir));
        homeDir.delete();
        assertTrue(root.list().isEmpty());
    }
}
