import FileSystem.Directory;
import FileSystem.Entity;
import FileSystem.Pipe;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("Named pipe")
public class PipeTest {
    @Test
    @DisplayName("Create pipe")
    void createFileTest() {
        assertThrows(IllegalArgumentException.class, () -> Pipe.create("test", null));

        Directory dir = Directory.create("some", null);
        assertThrows(IllegalArgumentException.class, () -> Pipe.create("", dir));

        Pipe file = Pipe.create("file sample", dir);
        assertNotNull(file);
        assertEquals(Entity.EntityType.NAMED_PIPE, file.getType());
        assertEquals("file sample", file.getName());
        assertFalse(file.isDirectory());
    }

    @Test
    @DisplayName("Rename")
    void renameTest() {
        Directory dir = Directory.create("dir", null);
        Pipe file = Pipe.create("file", dir);

        file.rename("file new_name");
        assertEquals("file new_name", file.getName());
        assertThrows(IllegalArgumentException.class, () -> file.rename(""));
    }

    @Test
    @DisplayName("Read write")
    void readWriteTest() {
        Directory dir = Directory.create("dir", null);
        Pipe file = Pipe.create("file", dir);

        assertEquals("", file.read());
        file.write("some data ");
        assertEquals("some data ", file.read());
        assertThrows(IllegalArgumentException.class, () -> file.write(null));
    }
}
