import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import FileSystem.Directory;
import FileSystem.Entity;
import FileSystem.TFileLog;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Log text file")
public class TFileLogTest {
    @Test
    @DisplayName("Create file")
    void createFileTest() {
        assertThrows(IllegalArgumentException.class,
                () -> TFileLog.create("test", null, "data"));

        Directory dir = Directory.create("some", null);
        assertThrows(IllegalArgumentException.class, () -> TFileLog.create("name", dir, null));

        TFileLog file = TFileLog.create("file sample", dir, "line");
        assertNotNull(file);
        assertEquals(Entity.EntityType.LOG_TEXT_FILE, file.getType());
        assertEquals("file sample", file.getName());
        assertFalse(file.isDirectory());
    }

    @Test
    @DisplayName("Read append")
    void readAppendTest() {
        Directory dir = Directory.create("dir", null);
        TFileLog file = TFileLog.create("file", dir, "data some");

        assertEquals("data some", file.read());
        file.append("\nanother data");
        assertEquals("data some\nanother data", file.read());
        file.append("");
        assertEquals("data some\nanother data", file.read());
    }
}
